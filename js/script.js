// game code
var WIDTH	= 940,
	HEIGHT	= 580,
	UP_ARROW	= 38;
	DOWN_ARROW	= 40;


var pi = Math.PI;
var canvas, ctx, keystate;
var player, ai, ball;
var score = {
	'player': 0,
	'ai': 0
};

var soundEnabled = true;
var soundList = {
	'goal-sound':	document.getElementById("goal-sound"),
	'ping-sound':	document.getElementById("ping-sound"),
	'loose-sound':	document.getElementById("loose-sound")
};

player = {
	x: null,
	y: null,
	width: 20,
	height: 100,

	update: function() {
		if(keystate[UP_ARROW]) {
			this.y -= 7;
		}

		if(keystate[DOWN_ARROW]) {
			this.y += 7;
		}
		this.y = Math.max( Math.min(this.y, HEIGHT - this.height), 0 );
	},
	draw: function() {
		ctx.fillRect(this.x, this.y, this.width, this.height)
	}
};
ai = {
	x: null,
	y: null,
	width: 20,
	height: 100,
	difficultyModifier: 0.1,

	update: function() {
		var dest = ball.y - (this.height - ball.side) * 0.5;
		this.y += (dest - this.y) * this.difficultyModifier;
		this.y = Math.max( Math.min(this.y, HEIGHT - this.height), 0 );
	},
	draw: function() {
		ctx.fillRect(this.x, this.y, this.width, this.height)
	}
};
ball = {
	x: null,
	y: null,
	velocity: null,
	side: 20,
	speed: 12,
	serve: function(side) {		

		var r = ( score['player'] === 0 && score['ai'] === 0 ) ? 0.5 : Math.random();

		this.x = (side == 1) ? (player.x + player.width) : (ai.x - this.side);
		this.y = (HEIGHT - this.side) * r;

		var phi = 0.1 * pi * (1 - 2*r);
		this.velocity = {
			x: side * this.speed * Math.cos(phi),
			y: this.speed * Math.sin(phi)
		};
		
	},
	update: function() {

		if ( this.x === null || this.y === null ) {
			return false;
		}

		this.x += this.velocity.x;
		this.y += this.velocity.y;

		if (0 > this.y || this.y + this.side > HEIGHT) {
			var offset = (this.velocity.y < 0) ? (0 - this.y) : (HEIGHT - (this.y + this.side));
			this.y += 2 * offset;
			this.velocity.y *= -1;
		}

		var AABBIntersect = function(ax, ay, aw, ah, bx, by, bw, bh) {
			return (ax < bx+bw) && (ay < by+bh) && (bx < ax+aw) && (by < ay+ah);
		};

		// decides the paddle object to colide with based on the direction
		var paddle = this.velocity.x < 0 ? player : ai;

		// if collided with paddle
		if ( AABBIntersect(paddle.x, paddle.y, paddle.width, paddle.height, this.x, this.y, this.side, this.side) ) {
			this.x = (paddle === player) ? player.x + player.width : ai.x - this.side;
			var n = (this.y + this.side - paddle.y) / (paddle.height + this.side);
			var phi = 0.25 * pi * (2*n - 1);

			var smash = (Math.abs(phi) > 0.2 * pi) ? 1.5 : 1;
			this.velocity.x = smash * (paddle === player ? 1 : -1) * this.speed * Math.cos(phi);
			this.velocity.y = smash * this.speed * Math.sin(phi);

			playSound('ping');
		}

		// if ball had fallen
		if (0 > this.x + this.side || this.x > WIDTH) {

			// serving on side of loser
			this.serve(paddle === player ? 1 : -1);

			// increment winner's score
			score[ (paddle === player ? 'ai' : 'player') ] += 1;
			playSound( (paddle === player) ? 'loose' : 'goal' );
			updateScore();
		}
	},
	draw: function() {
		ctx.fillRect(this.x, this.y, this.side, this.side)
	}
};

function main() {

	canvas = $('#canvas');
	canvas.attr('width', WIDTH);
	canvas.attr('height', HEIGHT);
	ctx = canvas[0].getContext('2d');
	canvas.css('visibility', 'visible');

	keystate = {};
	document.addEventListener('keydown', function(evt) {
		keystate[evt.keyCode] = true;
	});
	document.addEventListener('keyup', function(evt) {
		delete keystate[evt.keyCode];
	});

	init();

	var loop = function() {
		update();
		draw();

		window.requestAnimationFrame(loop, canvas);
	};
	window.requestAnimationFrame(loop, canvas);
}

function init() {
	player.x = player.width;
	player.y = (HEIGHT - player.height)/2;

	ai.x = WIDTH - (player.width + ai.width);
	ai.y = (HEIGHT - ai.height)/2;

	ball.serve(1);
}

function update() {
	ball.update();
	player.update();
	ai.update();
}

function draw() {
	ctx.fillRect(0, 0, WIDTH, HEIGHT);
	ctx.save();

	// white paddles
	ctx.fillStyle = '#fff';
	player.draw();
	ai.draw();

	// white middle stripe
	// var w = 4;
	// var x = (WIDTH - w) * 0.5;
	// var y = 0;
	// var step = HEIGHT/15;

	// while ( y < HEIGHT ) {
	// 	ctx.fillRect(x, y + (step * 0.25), w, (step * 0.5) );
	// 	y += step;
	// }

	// yellow ball
	ctx.fillStyle = '#f6ee31';
	ball.draw();

	ctx.restore();

}

function updateScore() {

	function addZero(n) {
		return (n<10) ? '0'+n : n;
	}

	$('#player-score').text( addZero(score['player']) );
	$('#ai-score').text( addZero(score['ai']) );

	if ( score['player'] == 9 ) {
		ai.difficultyModifier = 1;
	}
}

function playSound(sound) {
	if( soundEnabled && soundList[sound + "-sound"] ) {
		soundList[sound + "-sound"].volume = 0.5;
		soundList[sound + "-sound"].play();
	}
}

main();


// DOM manipulation
$(function() {

	document.addEventListener('keydown', function(evt) {
		if ( 38 === evt.keyCode || 40 == evt.keyCode ) {
			$('#content').css('opacity', 0.25);
		}
	});

	document.addEventListener('mousemove', function(e) {
		$('#content').css('opacity', 1);
	});

	// click on big balls
	$('#ball-links a').click('click', function() {
		$('#ball-links').hide();
		$('#small-ball-links').show();
		$( '#small-' + $(this).data('target') + '-link' ).trigger('click');

	});

	// click on small football link
	$('#small-football-link').on('click', function() {
		$('#volleyball-buttons, #volleyball-buttons .wrapper-table').hide();
		$('#football-buttons').show();
		$('#football-buttons > #btn-football-group-a').trigger('click');
		$('#small-ball-links a').removeClass('active');
		$(this).addClass('active');
	});

	// click on small volleyball link
	$('#small-volleyball-link').on('click', function() {
		$('#football-buttons, #football-buttons .wrapper-table').hide();
		$('#volleyball-buttons').show();
		$('#volleyball-buttons > #btn-volleyball-group-a').trigger('click');
		$('#small-ball-links a').removeClass('active');
		$(this).addClass('active');
	});

	// click on football buttons
	$('#football-buttons a').on('click', function() {
		$('#football-buttons a').removeClass('active');
		$(this).toggleClass('active');

		$('table.wrapper-table').hide();

		if ( $(this).data('target') ) {
			$($(this).data('target')).show();
		}

	});

	// click on volleyball buttons
	$('#volleyball-buttons a').on('click', function() {
		$('#volleyball-buttons a').removeClass('active');
		$(this).toggleClass('active');

		$('table.wrapper-table').hide();

		if ( $(this).data('target') ) {
			$($(this).data('target')).show();
		}
	});

	$('#sound-icon').on('click', function() {
		if( soundEnabled ) {
			soundEnabled = false;
		}
		else {
			soundEnabled = true;
		}

		if ( $(this).hasClass('on') ) {
			$(this).removeClass('on').addClass('off');
		}
		else {
			$(this).removeClass('off').addClass('on');
		}
	});
});
